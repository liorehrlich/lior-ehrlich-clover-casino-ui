![picture](images/libraryname.PNG)

lior-ehrlich-casino-clover-ui is available as an npm package.

This library contains building UI building blocks for your gaming website. 

[![NPM](https://img.shields.io/npm/v/lior-ehrlich-casino-clover-ui.svg)](https://www.npmjs.com/package/lior-ehrlich-casino-clover-ui) [![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

## Install

```bash
npm install --save lior-ehrlich-casino-clover-ui
yarn add lior-ehrlich-casino-clover-ui
```

## Storybook

https://heuristic-swartz-b31083.netlify.com/?path=/story/welcome-to-casino-clover-ui--intro

## Usage

```jsx
import React, { Component } from 'react'
import { CloverButton } from 'lior-ehrlich-casino-clover-ui'

function App() {
  return (
    <CloverButton variant="square" textColor="dark">
      Hello World
    </CloverButton>
  );
}
```

![picture](images/helloworld.PNG)



### Include components

```jsx
import { CloverButton, CloverHeader, CloverModal, CloverLoading } from 'lior-ehrlich-casino-clover-ui'
```

### Clover Button

Attribute | Type | Default | Description 
--- | --- | --- | --- 
variant | square, circle | square | shape of the button.
textColor | dark, bright | dark | font color of button.
disabled | boolean | false | is the button disabled.

### Clover Header

Attribute | Type | Default | Description 
--- | --- | --- | --- 
withStartIcon | square, circle | square | shape of the button.
withEndIcon | dark, bright | dark | font color of button.
fontSize | string | medium | font size of header.
fontColor | string | white | font color of header.
withShadow | boolean | false | creates shadowing for font.
ml | string | 0 | margin left in pixels.
mr | string | 0 | margin right in pixels.
mt | string | 0 | margin top in pixels.
mb | string | 0 | margin bottom in pixels.

### Clover Modal

Attribute | Type | Default | Description 
--- | --- | --- | --- 
open | boolean | false | Is the modal open or not. Best to save in parent component's state.
onClose | function | null | executed function when clicking outside the modal, or the ESC key.
children | component | null | The content of the modal.
className | string | null | add a class to add further style to the modal.

### Clover Loading

ExampleUsage:

```jsx
import React, { Component } from 'react'
import { CloverLoading } from 'lior-ehrlich-casino-clover-ui'

function App() {
  if (!results) {
    return (
        <CloverLoading />
    );
  }
  return (
    <ResultsWrapper>
      {results}
    </ResultsWrapper>
  );
}
```

### Clover Marquee

ExampleUsage:

```jsx
import React, { Component } from 'react'
import { CloverMarquee } from 'lior-ehrlich-casino-clover-ui'

function App() {
  return (
    <CloverMarquee>
      {results}
    </CloverMarquee>
  );
}
```

## License

MIT © [liorEhrlich](https://github.com/liorEhrlich)
