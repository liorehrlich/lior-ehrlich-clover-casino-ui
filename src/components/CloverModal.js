import React, { useRef, useEffect } from 'react'
import styled from 'styled-components'
import { theme } from '../css/theme'

export function CloverModal({ open, onClose, children, className }) {
  const handleOutsideClick = useDialogCloseHandler(onClose);
  return (
    <Dialog {...handleOutsideClick} className={className} open={open}>
      <StyledFlex>
        {children}
      </StyledFlex>
    </Dialog>
  )
}

function useDialogCloseHandler(cb) {
  const nodeRef = useRef();
  useEffect(() => {
    const { current: el } = nodeRef;
    const onEvent = event => {
      const { target, key } = event;
      if (key && key.toLowerCase() !== 'escape') {
        return
      }
      if (target === el || el.contains(target)) {
        return
      }
      cb()
    }
    document.addEventListener('mousedown', onEvent, true);
    document.addEventListener('keydown', onEvent, true);
    return () => {
      document.removeEventListener('mousedown', onEvent, true);
      document.removeEventListener('keydown', onEvent, true);
    }
  }, [cb])
  return { ref: nodeRef }
}

const Dialog = styled.dialog`
  top: 0;
  bottom: 0;
  border: none;
  background-color: ${props => props.theme.palette.body};
  color: ${props => props.theme.palette.text};
  box-shadow: 4px 4px 5px 0px rgba(0, 0, 0, 0.59);
`

const StyledFlex = styled.div`
  height: 100%
  padding: 5px
  flex-direction: column
  align-items: center
  display: flex
`

Dialog.defaultProps = {
  theme: theme
}