import React from 'react';
import { render } from '@testing-library/react';
import { CloverButton } from './CloverButton'

describe('CloverButton', () => {
  it('basic clover button snapshot test', () => {
    const { container, getByText } = render(<CloverButton>foo bar</CloverButton>)
    expect(getByText('foo bar')).toBeInTheDocument()
    expect(container).toMatchSnapshot()
  })

  it('clover button is circle and font color is dark', () => {
    const { container, getByText } = render(<CloverButton variant='circle' textColor='dark'>foo bar</CloverButton>)
    expect(getByText('foo bar')).toBeInTheDocument()
    expect(container).toMatchSnapshot()
  })

  it('clover button is disabled', () => {
    const { getByText } = render(<CloverButton disabled>foo bar</CloverButton>)
    expect(getByText('foo bar')).toBeDisabled()
  })
})
