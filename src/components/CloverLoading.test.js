import React from 'react';
import { render } from '@testing-library/react';
import { CloverLoading } from './CloverLoading'

describe('CloverLoading', () => {
  it('basic snapshot test', () => {
    const { container } = render(<CloverLoading />)
    expect(container).toMatchSnapshot()
  })
})
