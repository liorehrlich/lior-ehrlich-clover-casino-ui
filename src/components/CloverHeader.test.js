import React from 'react';
import { render } from '@testing-library/react';
import { CloverHeader } from './CloverHeader'

describe('CloverHeader', () => {
  it('clover header is shown, and matches snapshot', () => {
    const { container, getByText } = render(<CloverHeader>foo bar</CloverHeader>)
    expect(getByText('foo bar')).toBeInTheDocument()
    expect(container).toMatchSnapshot()
  })

  it('clover header test with props are shown, and matches snapshot', () => {
    const { container, getByText } = render(<CloverHeader fontSize='large' withStartIcon>foo bar</CloverHeader>)
    expect(getByText('🍀')).toBeInTheDocument()
    expect(container).toMatchSnapshot()
  })
})
