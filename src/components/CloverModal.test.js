import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import { CloverModal } from './CloverModal'

describe('CloverModal', () => {
  it('basic snapshot test', () => {
    const { container } = render(<CloverModal open>foo bar</CloverModal>)
    expect(container).toMatchSnapshot()
  })

  it('modal not having open prop shouldnt be displayed', () => {
    const { getByText } = render(<CloverModal>foo bar</CloverModal>)
    expect(getByText('foo bar')).not.toBeVisible()
  })

  it('modal having open prop should be displayed', () => {
    const { getByText } = render(<CloverModal open>foo bar</CloverModal>)
    expect(getByText('foo bar')).toBeVisible()
  })

  it('modal gets a function that is fired on escape key press', () => {
    render(<CloverModal open onClose={() => console.log('foo')}>foo bar</CloverModal>)
    jest.spyOn(console, 'log')
    fireEvent.keyDown(document.body, { key: "Escape", keyCode: 27, which: 27 })
    expect(console.log.mock.calls.length).toBe(1);
  })
})
