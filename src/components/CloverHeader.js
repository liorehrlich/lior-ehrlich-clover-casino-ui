import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

export function CloverHeader(props) {
  const {children, withStartIcon, withEndIcon, fontSize, fontColor, withShadow, htmlTag, mt, mb, ml, mr} = props
  return (
    <StyledCloverHeader as={htmlTag} fontSize={fontSize} fontColor={fontColor} withShadow={withShadow} mt={mt} mb={mb} ml={ml} mr={mr}>
      {withStartIcon && <span role='img' aria-label='Clover'>🍀 </span>}
      {children}
      {withEndIcon && <span role='img' aria-label='Clover'> 🍀</span>}
    </StyledCloverHeader>
  )
}

CloverHeader.defaultProps = {
  withStartIcon: false,
  withEndIcon: false,
  fontSize: 'medium',
  fontColor: 'black',
  withShadow: false,
  mt: 0,
  mb: 0,
  ml: 0,
  mr: 0
}

CloverHeader.propTypes = {
  withStartIcon: PropTypes.bool,
  withEndIcon: PropTypes.bool,
  fontSize: PropTypes.oneOf(['x-small', 'small', 'medium', 'large']),
  fontColor: PropTypes.string,
  withShadow: PropTypes.bool,
  mt: PropTypes.number,
  mb: PropTypes.number,
  ml: PropTypes.number,
  mr: PropTypes.number,
}

const StyledCloverHeader = styled.span`
    font-size: ${props => props.fontSize && props.fontSize === 'large' ? '8vw' : props.fontSize === 'medium' ? '6vw' : props.fontSize === 'small' ? '4vw' : '2vw'};
    color: ${props => props.fontColor};
    text-shadow: ${props => props.withShadow && '1px 1px 20px black'};
    margin-top: ${props => props.mt};
    margin-bottom: ${props => props.mb};
    margin-left: ${props => props.ml};
    margin-right: ${props => props.mr};
`