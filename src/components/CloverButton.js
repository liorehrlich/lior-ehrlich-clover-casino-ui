import PropTypes from 'prop-types';
import styled from "styled-components";
import { theme } from '../css/theme'

export const CloverButton = styled.button`
  width: inherit;
  padding: ${props => props.size && props.size === 'large' ? '50px' : '20px'};
  font-size: ${props => props.size && props.size === 'large' ? '6vw' : '2vw'};
  border-radius: ${props => props.variant && props.variant === 'circle' ? '100%' : '10px'};
  align-self: stretch;
  font-weight: bold;
  background-color: #006600;
  color: ${props => props.textColor && props.textColor.toLowerCase() === 'dark' ? props.theme.palette.primaryTextDark : props.theme.palette.primaryTextBright};
  &[disabled] {
    opacity: 0.4;
  }
`

CloverButton.defaultProps = {
  theme: theme,
  textColor: 'dark',
  disabled: false,
  variant: 'square',
  size: 'medium'
}

CloverButton.propTypes = {
  textColor: PropTypes.oneOf(['dark', 'bright']),
  disabled: PropTypes.bool,
  variant: PropTypes.oneOf(['square', 'circle']),
  size: PropTypes.oneOf(['medium', 'large'])
}

