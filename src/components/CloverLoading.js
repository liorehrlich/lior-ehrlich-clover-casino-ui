import React from 'react'
import styled, { keyframes } from 'styled-components'
import { theme } from '../css/theme'

export function CloverLoading() {
  return (
    <div>
      <LoadingBar />
      <LoadingBar />
      <LoadingBar />
      <LoadingBar />
    </div>
  )
}

const cloverLoading = keyframes`
  0% {
    transform: scale(1);
  }
  20% {
    transform: scale(1, 2.2);
  }
  40% {
    transform: scale(1);
  }
`

const LoadingBar = styled.div`
  display: inline-block;
  width: 4px;
  height: 18px;
  border-radius: 4px;
  background-color: ${props => props.theme.palette.primary};
  margin: 0 5px;
  animation: ${cloverLoading} 1s ease-in-out infinite;

  &:nth-child(1) {
    animation-delay: 0;
  }
  &:nth-child(2) {
    animation-delay: 0.09s;
  }
  &:nth-child(3) {
    animation-delay: 0.18s;
  }
  &:nth-child(4) {
    animation-delay: 0.27s;
  }
`

LoadingBar.defaultProps = {
  theme: theme
}