import React from "react";
import styled from "styled-components";

export function CloverMarquee ({ children }) {
  return <MarqueeWrapper>
        <MarqueeAnimtion>
            {children}
        </MarqueeAnimtion>
    </MarqueeWrapper>
}

const MarqueeWrapper = styled.span`
  white-space: nowrap;
  overflow: hidden;
`;

const MarqueeAnimtion = styled.div`
  padding-left: 100%;
  padding-right: 100%;
  animation: marquee 15s linear infinite;
  @keyframes marquee {
    0%{
      transform: translate(0, 0);
    }
    100%{
      transform: translate(-100%, 0);
    }
  }
`;

