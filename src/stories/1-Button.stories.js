import React from 'react';
import styled from "styled-components";
import { action } from '@storybook/addon-actions';
import { CloverButton } from "../components/CloverButton";



export default {
  title: 'CloverButton',
};


export const basic = () =>
  <Wrapper>
    <CloverButton onClick={action('clicked')}>Basic Button</CloverButton>
    <span>Story Source: </span>
    <StorySource>
      &lt;CloverButton&gt;
      Basic Button
      &lt;/CloverButton&gt;</StorySource>
    <CloverButtonPropsTable />
  </Wrapper>;

export const circle = () =>
  <Wrapper>
    <CloverButton variant='circle'
      onClick={action('clicked')}>Circle Button</CloverButton>
    <span>Story Source: </span>
    <StorySource>
      &lt;CloverButton variant='circle'&gt;
      Circle Button
      &lt;/CloverButton&gt;</StorySource>
    <CloverButtonPropsTable />
  </Wrapper>;

export const bright = () =>
  <Wrapper>
    <CloverButton textColor='bright'
      onClick={action('clicked')}>Bright Button</CloverButton>
    <span>Story Source: </span>
    <StorySource>
      &lt;CloverButton textColor='bright'&gt;
      Bright Button
      &lt;/CloverButton&gt;</StorySource>
    <CloverButtonPropsTable />
  </Wrapper>;

export const disabled = () =>
  <Wrapper>
    <CloverButton disabled
      onClick={action('clicked')}>Disabled Button</CloverButton>
    <span>Story Source: </span>
    <StorySource>
      &lt;CloverButton disabled&gt;
      Disabled Button
      &lt;/CloverButton&gt;</StorySource>
    <CloverButtonPropsTable />
  </Wrapper>;

const CloverButtonPropsTable = () => {
  return (
    <TableWrapper>
      <table border='1'>
        <HeaderRow>
          <th>Attribute</th>
          <th>Type</th>
          <th>Default</th>
          <th>Description</th>
        </HeaderRow>
        <Row>
          <td>variant</td>
          <td>square</td>
          <td>circle square</td>
          <td>shape of the button</td>
        </Row>
        <Row>
          <td>textColor</td>
          <td>dark</td>
          <td>bright dark</td>
          <td>font color of button</td>
        </Row>
        <Row>
          <td>size</td>
          <td>medium</td>
          <td>medium large</td>
          <td>size of button and font</td>
        </Row>
        <Row>
          <td>disabled</td>
          <td>boolean</td>
          <td>false</td>
          <td>is button disabled</td>
        </Row>
      </table>
    </TableWrapper>
  )
}

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  width: 500px;
  font-family: Sans-serif	
`;

const TableWrapper = styled.div`
  margin-top: 15px;
`;

const HeaderRow = styled.tr`
  background-color: darkcyan;
`;

const Row = styled.tr`
  background-color: cadetblue;
`;

const StorySource = styled.p`
  padding: 5px;
  border: 2px solid green;
  display: inline-block;
  width: fit-content;
  border-radius: 5px;
`;