import React, { useState } from 'react';
import styled from 'styled-components'

import { CloverModal } from "../components/CloverModal"
import { CloverButton } from "../components/CloverButton"
import { CloverHeader } from "../components/CloverHeader"

export default {
  title: 'CloverModal',
};

const ModalWrapper = () => {
  const [isOpen, setIsOpen] = useState(true)
  return <div>
    <CloverModal open={isOpen}>
      This is the modal
    </CloverModal>
    <button onClick={() => setIsOpen(false)}>Close Modal</button>
    <button onClick={() => setIsOpen(true)}>Open Modal</button>
  </div>;
}

export const basic = () =>
  <Wrapper>
    <h1>Modal</h1>
    <span>Story Source: </span>
    <StorySource>
      &lt;CloverModal open=&#123;isOpen&#125;&gt; <br/>
      This is the modal<br/>
      &lt;/CloverModal&gt;</StorySource>
    <ModalWrapper/>
  </Wrapper>;

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  width: 500px;
  font-family: Sans-serif	
`;

const StorySource = styled.p`
  padding: 5px;
  border: 2px solid green;
  display: inline-block;
  width: fit-content;
  border-radius: 5px;
`;