import React from 'react';
import styled from "styled-components";

import { CloverMarquee } from "../components/CloverMarquee";

export default {
  title: 'CloverMarquee',
};

export const basic = () =>
  <Wrapper>
    <h1>Marquee</h1>
    <span>Story Source: </span>
    <StorySource>
      &lt;CloverMarquee&gt; <br/>
      This is a marquee title <br/>
      &lt;/CloverMarquee&gt;</StorySource>
    <CloverMarquee>
      This is a marquee title
    </CloverMarquee>
  </Wrapper>;

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  width: 300px;
  font-family: Sans-serif	
`;

const StorySource = styled.p`
  padding: 5px;
  border: 2px solid green;
  display: inline-block;
  width: fit-content;
  border-radius: 5px;
`;