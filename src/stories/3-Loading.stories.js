import React from 'react';
import styled from 'styled-components'

import { CloverLoading } from "../components/CloverLoading"

export default {
  title: 'CloverLoading',
};

export const basic = () =>
  <Wrapper>
    <h1>Loading mask</h1>
    <span>Story Source: </span>
    <StorySource>
      &lt;CloverLoading/&gt;</StorySource>
    <CloverLoading />
  </Wrapper>;

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  width: 500px;
  font-family: Sans-serif	
`;

const StorySource = styled.p`
  padding: 5px;
  border: 2px solid green;
  display: inline-block;
  width: fit-content;
  border-radius: 5px;
`;