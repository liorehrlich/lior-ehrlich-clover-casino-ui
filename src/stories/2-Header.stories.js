import React from 'react';
import styled from "styled-components";
import { CloverHeader } from "../components/CloverHeader"

export default {
  title: 'CloverHeader',
};

export const basic = () =>
  <Wrapper>
    <CloverHeader>Basic Header</CloverHeader>
    <span>Story Source: </span>
    <StorySource>
      &lt;CloverHeader&gt;
      Basic Header
      &lt;/CloverHeader&gt;</StorySource>
    <CloverHeaderPropsTable />
  </Wrapper>;

export const withStartIcon = () =>
  <Wrapper>
    <CloverHeader withStartIcon>With Start Icon Header</CloverHeader>
    <span>Story Source: </span>
    <StorySource>
      &lt;CloverHeader withStartIcon&gt;
      With Start Icon Header
      &lt;/CloverHeader&gt;</StorySource>
    <CloverHeaderPropsTable />
  </Wrapper>;

export const withEndIcon = () =>
  <Wrapper>
    <CloverHeader withEndIcon>With End Icon Header</CloverHeader>
    <span>Story Source: </span>
    <StorySource>
      &lt;CloverHeader withEndIcon&gt;
      With End Icon Header
      &lt;/CloverHeader&gt;</StorySource>
    <CloverHeaderPropsTable />
  </Wrapper>;

export const smallFont = () =>
  <Wrapper>
    <CloverHeader fontSize='small'>Small Header</CloverHeader>
    <span>Story Source: </span>
    <StorySource>
      &lt;CloverHeader fontSize='small'&gt;
      Small Header
      &lt;/CloverHeader&gt;</StorySource>
    <CloverHeaderPropsTable />
  </Wrapper>;

export const bigFont = () =>
  <Wrapper>
    <CloverHeader fontSize='large'>Large Header</CloverHeader>
    <span>Story Source: </span>
    <StorySource>
      &lt;CloverHeader fontSize='large'&gt;
      Large Header
      &lt;/CloverHeader&gt;</StorySource>
    <CloverHeaderPropsTable />
  </Wrapper>;

export const withoutShadow = () =>
  <Wrapper>
    <CloverHeader withShadow={false} fontColor='green'>Without shadowing</CloverHeader>
    <span>Story Source: </span>
    <StorySource>
      &lt;CloverHeader withShadow=&#123;false&#125;&gt;
      Without Shadowing
      &lt;/CloverHeader&gt;</StorySource>
    <CloverHeaderPropsTable />
  </Wrapper>;

const CloverHeaderPropsTable = () => {
  return (
    <TableWrapper>
      <table border='1'>
        <HeaderRow>
          <th>Attribute</th>
          <th>Type</th>
          <th>Default</th>
          <th>Description</th>
        </HeaderRow>
        <Row>
          <td>withStartIcon</td>
          <td>string</td>
          <td>false</td>
          <td>add clover icon to start of header</td>
        </Row>
        <Row>
          <td>withEndIcon</td>
          <td>string</td>
          <td>false</td>
          <td>add clover icon to end of header</td>
        </Row>
        <Row>
          <td>fontSize</td>
          <td>string</td>
          <td>medium</td>
          <td>x-small, small, medium, large</td>
        </Row>
        <Row>
          <td>fontColor</td>
          <td>string</td>
          <td>white</td>
          <td>changes header color</td>
        </Row>
        <Row>
          <td>withShadow</td>
          <td>boolean</td>
          <td>true</td>
          <td>adds shadowing</td>
        </Row>
      </table>
    </TableWrapper>
  )
}

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  width: 500px;
  font-family: Sans-serif	
`;

const TableWrapper = styled.div`
  margin-top: 15px;
`;

const HeaderRow = styled.tr`
  background-color: darkcyan;
`;

const Row = styled.tr`
  background-color: cadetblue;
`;

const StorySource = styled.p`
  padding: 5px;
  border: 2px solid green;
  display: inline-block;
  width: fit-content;
  border-radius: 5px;
`;