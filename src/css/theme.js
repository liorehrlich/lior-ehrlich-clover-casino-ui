const secondary = "#FFFAFA";
const primary = "#2db14b";
const bgTextBright = "#f6e9e9";
const bgTextDark = "#000000";
const positive = "#305A38";
const negative = "#ca0303";

export const theme = {
  palette: {
    body: "#363333",
    text: bgTextBright,
    primary,
    primaryTextBright: bgTextBright,
    primaryTextDark: bgTextDark,
    secondary,
    inverse: bgTextBright,
    inverseText: "#363333",
    positive,
    negative,
    dark: "rgb(54, 51, 51)"
  },
  fonts: {
    main: `-apple-system, BlinkMacSystemFont, "Segoe UI", "Roboto", "Oxygen",
    "Ubuntu", "Cantarell", "Fira Sans", "Droid Sans", "Helvetica Neue",
    sans-serif`,
    code: `source-code-pro, Menlo, Monaco, Consolas, "Courier New",
    monospace`
  }
};
